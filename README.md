# Food to Share

This application is an application for the delivery of meals, an association registers food delivery routes, and within this route contains points, in the points we have available a set of benefits that will receive these meals.

## Frontend
The frontend of this application was developed in [Flutter](https://flutter.dev/docs).

## Backend
The backend of this application was developed in [Golang](https://go.dev/).

## Important points to highlight

> - Multi language, currently Portuguese and English;
> - Navigation through the Google Maps app;