class BeneficiaryModel {
  final String accessToken;
  final String refreshToken;

  BeneficiaryModel(this.accessToken, this.refreshToken);
}
