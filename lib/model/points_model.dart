class PointsModel {
  final int id;
  final String name;
  final String description;
  final int routId;
  final num latitude;
  final num longitude;
  bool firstDelivery;
  final String addressCorrectionComment;

  PointsModel(this.id, this.name, this.routId, this.latitude, this.longitude,
      this.description, this.firstDelivery, this.addressCorrectionComment);
}
