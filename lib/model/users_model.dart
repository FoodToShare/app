class UserModel {
  final int id;
  final String email;
  final String role;

  UserModel(this.id, this.email, this.role);
}
