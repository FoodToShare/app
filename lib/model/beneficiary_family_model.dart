import 'package:app/model/beneficiary_model.dart';

class BeneficiaryFamilyModel {
  final int familyGroup;
  final List<BeneficiaryModel> beneficiaries;
  bool received;

  BeneficiaryFamilyModel(this.familyGroup, this.beneficiaries, this.received);
}
