import 'package:app/model/users_model.dart';

class RoutesModel {
  final int id;
  final String name;
  final String description;
  final List deliveryPoints;
  final UserModel responsible;

  RoutesModel(this.id, this.name, this.description, this.deliveryPoints,
      this.responsible);
}
