// ignore_for_file: unused_local_variable

class BeneficiaryModel {
  final int id;
  final String firstName;
  final String lastName;
  final String socialSecurity;
  final String cellPhone;
  final String address;
  final int deliveryPointId;
  final int familyGroup;

  BeneficiaryModel(this.id, this.firstName, this.lastName, this.socialSecurity,
      this.cellPhone, this.address, this.deliveryPointId, this.familyGroup);
}
