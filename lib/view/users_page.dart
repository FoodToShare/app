// ignore_for_file: prefer_const_constructors

import 'package:app/widget/nav_bar.dart';
import 'package:app/widget/user/add_user.dart';
import 'package:app/widget/user/list_users.dart';
import 'package:flutter/material.dart';

class UsersPage extends StatefulWidget {
  const UsersPage({Key? key}) : super(key: key);

  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Users")),
      body: ListUsers(),
      drawer: NavBar(),
      floatingActionButton: AddUser(),
    );
  }
}
