// ignore_for_file: prefer_const_constructors, unrelated_type_equality_checks, prefer_final_fields, unused_field, prefer_const_literals_to_create_immutables, empty_catches, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:app/controller/login_controller.dart' as controller;
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

bool _isChecked = false;
TextEditingController _emailController = TextEditingController();
TextEditingController _passwordController = TextEditingController();

class _LoginPageState extends State<LoginPage> {
  String email = '';
  String password = '';
  @override
  void initState() {
    _loadUserEmailPassword();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                    width: 200,
                    height: 200,
                    child: Image.asset('assets/images/fts_Logo.png')),
                SizedBox(height: 35),
                field(_emailController, Icon(Icons.email_outlined), "Email",
                    true),
                SizedBox(height: 10),
                field(
                    _passwordController,
                    Icon(
                      Icons.lock_outline,
                    ),
                    "Password",
                    false),
                Row(children: [
                  Checkbox(
                      value: _isChecked,
                      onChanged: (value) {
                        _isChecked = !_isChecked;
                        SharedPreferences.getInstance().then(
                          (prefs) {
                            prefs.setBool("remember_me", _isChecked);
                            prefs.setString('email', _emailController.text);
                            prefs.setString(
                                'password', _passwordController.text);
                          },
                        );
                        setState(() {
                          _isChecked = _isChecked;
                        });
                      }),
                  SizedBox(width: 10.0),
                  Text("Remember Me")
                ]),
                SizedBox(height: 15),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30))),
                      onPressed: () async {
                        var resultLogin = await controller.LoginController()
                            .getLoginValues(_emailController.text,
                                _passwordController.text);
                        if (resultLogin == 'Success') {
                          Navigator.of(context).pushReplacementNamed('/routes');
                        } else if (resultLogin == 'Error') {
                          showToast('Invalid login!');
                        } else {
                          showToast('There is a ploblem with the system!');
                        }
                        //
                      },
                      child: Text('Login')),
                ),
                SizedBox(height: 100),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
  void _loadUserEmailPassword() async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      var _email = _prefs.getString("email") ?? "";
      var _password = _prefs.getString("password") ?? "";
      var _remeberMe = _prefs.getBool("remember_me") ?? false;
      if (_remeberMe) {
        setState(() {
          _isChecked = true;
        });
        _emailController.text = _email;
        _passwordController.text = _password;
      }
    } catch (e) {}
  }
}

void showToast(text) {
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 16.0);
}

field(TextEditingController controller, Icon icon, String label, bool email) {
  return Container(
    decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.white,
            blurRadius: 5.0,
          ),
        ],
        borderRadius: BorderRadius.circular(30),
        border: Border.all(color: Colors.grey)),
    child: TextField(
        controller: controller,
        keyboardType: email ? TextInputType.emailAddress : TextInputType.text,
        obscureText: !email,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(top: 8, left: 20),
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          border: InputBorder.none,
          suffixIcon: icon,
          labelText: label,
          labelStyle: TextStyle(fontSize: 14, decoration: TextDecoration.none),
        )),
  );
}
