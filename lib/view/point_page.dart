// ignore_for_file: prefer_const_constructors

import 'package:app/widget/nav_bar.dart';
import 'package:app/widget/point/add_points.dart';
import 'package:app/widget/point/list_points.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

bool page = true;

class PointPage extends StatefulWidget {
  const PointPage({Key? key}) : super(key: key);

  @override
  _PointPageState createState() => _PointPageState();
}

class _PointPageState extends State<PointPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Points"),
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  page = !page;
                  ListPoint().createState().changePageView();
                });
              },
              icon: page
                  ? Icon(FontAwesomeIcons.mapMarkedAlt)
                  : Icon(FontAwesomeIcons.list)),
        ],
      ),
      body: ListPoint(),
      drawer: NavBar(),
      floatingActionButton: AddPoint(),
    );
  }
}
