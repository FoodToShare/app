// ignore_for_file: prefer_const_constructors, unnecessary_this

import 'package:app/widget/beneficiary_family/beneficiary_family.dart';
import 'package:app/widget/nav_bar.dart';
import 'package:flutter/material.dart';

class BeneficiaryFamilyPage extends StatelessWidget {
  final int point;
  const BeneficiaryFamilyPage(this.point, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Beneficiaries"),
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: BeneficiaryFamily(this.point),
      drawer: NavBar(),
    );
  }
}
