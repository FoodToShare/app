// ignore: avoid_web_libraries_in_flutter
// ignore_for_file: prefer_const_constructors, unused_local_variable
import 'package:app/widget/route/add_route.dart';
import 'package:app/widget/list_simple.dart';
import 'package:app/widget/nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:app/controller/globals.dart' as globals;

class RoutePage extends StatelessWidget {
  const RoutePage({Key? key}) : super(key: key);
  // ignore: use_key_in_widget_constructors

  @override
  Widget build(BuildContext context) {
    if (globals.role == "VOLUNTARY") {
      return Scaffold(
        appBar: AppBar(title: Text("Routes")),
        body: Center(
          child: ListSimple("Routes", List.empty()),
        ),
        drawer: NavBar(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(title: Text("Routes")),
        body: Center(
          child: ListSimple("Routes", List.empty()),
        ),
        drawer: NavBar(),
        floatingActionButton: AddRoute(),
      );
    }
  }
}
