// ignore_for_file: prefer_const_constructors, unnecessary_this, unused_import

import 'package:app/widget/list_simple.dart';
import 'package:app/widget/nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PointsPage extends StatelessWidget {
  final List route;
  const PointsPage(this.route, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Points"),
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: Center(
        child: ListSimple("Points", this.route),
      ),
      drawer: NavBar(),
    );
  }
}
