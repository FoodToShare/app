// ignore_for_file: prefer_const_constructors, unused_element, avoid_print

import 'package:flutter/material.dart';
import 'package:app/widget/beneficiary/add_beneficiary.dart';
import 'package:app/widget/beneficiary/list_search.dart';
import 'package:app/widget/nav_bar.dart';

class BeneficiarioPage extends StatefulWidget {
  const BeneficiarioPage({Key? key}) : super(key: key);

  @override
  _BeneficiarioPageState createState() => _BeneficiarioPageState();
}

class _BeneficiarioPageState extends State<BeneficiarioPage> {
  Icon customIcon = Icon(Icons.search);
  Widget customSearchBar = Text('Beneficiaries');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: customSearchBar,
      ),
      body: Center(
        child: ListSearch(),
      ),
      drawer: NavBar(),
      floatingActionButton: AddBeneficiario(),
    );
  }
}
