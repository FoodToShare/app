// ignore_for_file: unused_import, unused_local_variable

import 'dart:convert';
import 'package:app/model/beneficiary_model.dart' as beneficiary_model;
import 'package:app/model/beneficiary_model.dart';
import 'package:http/http.dart' as http;
import 'package:app/controller/globals.dart' as globals;

class BeneficiaryController {
  Future<List<BeneficiaryModel>> getBenefs() async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/beneficiary/all');
      var response = await client.get(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<BeneficiaryModel> beneficiaries = [];
        for (var i in json) {
          BeneficiaryModel beneficiary = BeneficiaryModel(
              i['id'],
              i['first_name'],
              i['last_name'],
              i['social_security'],
              i['cell_phone'],
              i['address'],
              i['delivery_point_id'],
              i['family_group']);
          beneficiaries.add(beneficiary);
        }
        return beneficiaries;
      } else {
        //return "Error";
        return List.empty();
      }
    } catch (e) {
      throw Exception("Failed to get beneficiaries.");
    }
  }

  Future<String> createBeneficiary(
      String firstName,
      String lastName,
      String socialSecurity,
      String cellphone,
      String address,
      int pointID) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/beneficiary/');
      var response = await client.post(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
        body: jsonEncode({
          "first_name": firstName.toString(),
          "last_name": lastName.toString(),
          "social_security": socialSecurity.toString(),
          "cell_phone": cellphone.toString(),
          "delivery_point_id": pointID,
          "address": address.toString(),
        }),
      );
      if (response.statusCode == 201) {
        return "success";
      } else {
        return "error";
      }
    } catch (e) {
      return "SystemError";
    }
  }

  editBeneficiary(firstName, lastName, socialSecurity, cellphone, address,
      idPoint, beneficiaryID) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url =
          Uri.parse(globals.URI_API + '/api/v1/beneficiary/$beneficiaryID');
      var response = await client.patch(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
        body: jsonEncode({
          "first_name": firstName.toString(),
          "last_name": lastName.toString(),
          "social_security": socialSecurity.toString(),
          "cell_phone": cellphone.toString(),
          "delivery_point_id": idPoint,
          "address": address.toString(),
        }),
      );
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "error";
      }
    } catch (e) {
      return "SystemError";
    }
  }

  Future<String> deleteBeneficiary(int idBeneficiary) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url =
          Uri.parse(globals.URI_API + '/api/v1/beneficiary/$idBeneficiary');
      var response = await client.delete(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "error";
      }
    } catch (e) {
      throw Exception("error deleting user");
    }
  }
}
