// ignore_for_file: unused_import, prefer_typing_uninitialized_variables

import 'dart:convert';

import 'package:app/model/beneficiary_model.dart';
import 'package:app/model/users_model.dart';
import 'package:http/http.dart' as http;
import 'package:app/controller/globals.dart' as globals;

class UserController {
  Future<List<UserModel>> getUsers() async {
    try {
      final String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + "/api/v1/user/");
      var response = await client.get(url, headers: {
        "content-type": "application/json; charset=UTF-8",
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      });
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<UserModel> users = [];
        for (var i in json) {
          UserModel user = UserModel(i['id'], i['email'], i['role']);
          users.add(user);
        }
        return users;
      } else {
        return List.empty();
      }
    } catch (e) {
      throw Exception("Failed to get users.");
    }
  }

  Future<void> getRoleFromUser(int userID) async {
    try {
      final String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + "/api/v1/user/$userID");
      var response = await client.get(url, headers: {
        "content-type": "application/json; charset=UTF-8",
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      });
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        globals.role = json['role'];
        //perfect

      }
    } catch (e) {
      throw Exception("Failed to get users.");
    }
  }

  Future<String> createUser(String email, String password, String role) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/user/');

      var response = await client.post(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
        body: jsonEncode({"email": email, "password": password, "role": role}),
      );
      if (response.statusCode == 201) {
        return "success";
      } else {
        return "fail";
      }
    } catch (e) {
      throw Exception("Error creating a new route");
    }
  }

  Future<String> deleteUser(int idUser) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/user/$idUser');
      var response = await client.delete(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "error";
      }
    } catch (e) {
      throw Exception("error deleting user");
    }
  }

  Future<String> updateUser(
      String email, String password, String role, int userID) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/user/$userID');
      var body;
      if (password == "") {
        body = jsonEncode({"email": email, "role": role});
      } else {
        body = jsonEncode({"email": email, "password": password, "role": role});
      }
      var response = await client.patch(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
        body: body,
      );
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "fail";
      }
    } catch (e) {
      throw Exception("Error creating a new route");
    }
  }
}
