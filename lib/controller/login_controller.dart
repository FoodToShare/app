// ignore_for_file: avoid_print
import 'dart:convert';
import 'package:app/controller/user_controller.dart';
import 'package:http/http.dart' as http;
import 'package:app/controller/globals.dart' as globals;
import 'package:jwt_decode/jwt_decode.dart';

class LoginController {
  Future<String> getLoginValues(email, password) async {
    try {
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + "/api/v1/auth/login");
      var response = await client.post(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
        },
        body: jsonEncode({
          "id": 0,
          "email": email.toString(),
          "password": password.toString(),
        }),
      );
      if (response.statusCode == 200) {
        var jsonBody = jsonDecode(response.body);
        globals.email = email.toString();
        globals.access_token = jsonBody['accessToken'];
        globals.refresh_token = jsonBody['refreshToken'];
        Map<String, dynamic> payload = Jwt.parseJwt(jsonBody['accessToken']);
        globals.userID = payload["user_id"].toString();
        await UserController().getRoleFromUser(payload["user_id"]);
        return "Success";
      } else {
        return response.statusCode.toString();
      }
    } catch (e) {
      return "SystemError";
    }
  }

  Future<String> logout() async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + "/api/v1/auth/logout");
      var response = await client.post(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        globals.access_token = null;
        globals.refresh_token = null;
        return "Success";
      } else {
        return "Error";
      }
    } catch (e) {
      return "SystemError";
    }
  }
}
