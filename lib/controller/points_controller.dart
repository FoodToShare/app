// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';

import 'package:app/model/points_model.dart';
import 'package:http/http.dart' as http;
import 'package:app/controller/globals.dart' as globals;

class PointController {
  Future<List<PointsModel>> getPoints(String idRoute) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(
          globals.URI_API + '/api/v1/delivery/point/route/' + idRoute);
      var response = await client.get(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<PointsModel> points = [];
        for (var i in json) {
          PointsModel point = PointsModel(
              i['id'],
              i['name'],
              i['route_id'],
              i['latitude'],
              i['longitude'],
              i['description'],
              i['firstDelivery'],
              i['addressCorrectionComment']);
          points.add(point);
        }
        return points;
      } else {
        //return "Error";
        return List.empty();
      }
    } catch (e) {
      throw Exception("Failed to get routes.");
    }
  }

  Future<List<PointsModel>> getAllPoints() async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/point');
      var response = await client.get(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<PointsModel> points = [];
        for (var i in json) {
          PointsModel point = PointsModel(
              i['id'],
              i['name'],
              i['route_id'],
              i['latitude'],
              i['longitude'],
              i['description'],
              i['firstDelivery'],
              i['addressCorrectionComment']);
          points.add(point);
        }
        return points;
      } else {
        //return "Error";
        return List.empty();
      }
    } catch (e) {
      throw Exception("Failed to get routes.");
    }
  }

  Future addComentary(int idPoint, String comment) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/point/$idPoint');
      var response = await client.patch(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
        body: jsonEncode(
            {"firstDelivery": false, "addressCorrectionComment": comment}),
      );
      if (response.statusCode == 200) {
        return "Success";
      } else {
        return "Error";
      }
    } catch (e) {
      throw Exception("Failed to update comment.");
    }
  }

  Future<String> createPoint(String name, String description, String idRoute,
      String lat, String long) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/point/');
      var body;
      if (idRoute == "0") {
        body = jsonEncode({
          "name": name.toString(),
          "description": description.toString(),
          "latitude": double.tryParse(lat),
          "longitude": double.tryParse(long)
        });
      } else {
        body = jsonEncode({
          "name": name.toString(),
          "description": description.toString(),
          "latitude": double.tryParse(lat),
          "longitude": double.tryParse(long),
          "route_id": int.tryParse(idRoute)
        });
      }
      var response = await client.post(url,
          headers: {
            "content-type": "application/json; charset=UTF-8",
            "Accept": "application/json",
            "Authorization": "Bearer $token",
          },
          body: body);
      if (response.statusCode == 201) {
        return "success";
      } else {
        return "fail";
      }
    } catch (e) {
      throw Exception("Error creating a new route");
    }
  }

  Future<String> updatePoint(String name, String description, String idRoute,
      String lat, String long, String idPoint) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/point/$idPoint');
      var body;
      if (idRoute == "0") {
        body = jsonEncode({
          "name": name.toString(),
          "description": description.toString(),
          "latitude": double.tryParse(lat),
          "longitude": double.tryParse(long)
        });
      } else {
        body = jsonEncode({
          "name": name.toString(),
          "description": description.toString(),
          "latitude": double.tryParse(lat),
          "longitude": double.tryParse(long),
          "route_id": int.tryParse(idRoute)
        });
      }
      var response = await client.patch(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
        body: body,
      );
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "fail";
      }
    } catch (e) {
      throw Exception("Error creating a new route");
    }
  }

  Future<String> deletePoint(int idPoint) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/point/$idPoint');
      var response = await client.delete(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "error";
      }
    } catch (e) {
      throw Exception("error deleting user");
    }
  }
}
