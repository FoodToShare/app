// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';

import 'package:app/model/routes_model.dart';
import 'package:app/model/users_model.dart';
import 'package:http/http.dart' as http;
import 'package:app/controller/globals.dart' as globals;

class RouteController {
  Future<List<RoutesModel>> getRoutes() async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/route');
      var response = await client.get(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<RoutesModel> routes = [];
        for (var i in json) {
          var responsibleB = i['responsible'];
          UserModel responsible = UserModel(
              responsibleB['id'], responsibleB['email'], responsibleB['role']);
          RoutesModel route = RoutesModel(i['id'], i['name'], i['description'],
              i['delivery_points'], responsible);

          routes.add(route);
        }
        return routes;
      } else {
        //return "Error";
        return List.empty();
      }
    } catch (e) {
      throw Exception("Failed to get routes.");
    }
  }

  Future<String> createRoute(
      String name, String description, int userID) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/route/');
      var body;
      if (userID == 0) {
        body = jsonEncode(
            {"name": name.toString(), "description": description.toString()});
      } else {
        body = jsonEncode({
          "name": name.toString(),
          "description": description.toString(),
          "userID": userID
        });
      }
      var response = await client.post(url,
          headers: {
            "content-type": "application/json; charset=UTF-8",
            "Accept": "application/json",
            "Authorization": "Bearer $token",
          },
          body: body);
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "fail";
      }
    } catch (e) {
      throw Exception("Error creating a new route");
    }
  }

  Future<String> editRoute(
      String name, String description, int userID, int routeID) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/route/$routeID');
      var body;
      if (userID == 0) {
        body = jsonEncode({
          "name": name.toString(),
          "description": description.toString(),
          "userID": null
        });
      } else {
        body = jsonEncode({
          "name": name.toString(),
          "description": description.toString(),
          "userID": userID
        });
      }
      var response = await client.patch(url,
          headers: {
            "content-type": "application/json; charset=UTF-8",
            "Accept": "application/json",
            "Authorization": "Bearer $token",
          },
          body: body);
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "error";
      }
    } catch (e) {
      throw Exception("error deleting route");
    }
  }

  Future<String> deleteRoute(int idRoute) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/route/$idRoute');
      var response = await client.delete(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        return "success";
      } else {
        return "error";
      }
    } catch (e) {
      throw Exception("error deleting route");
    }
  }
}
