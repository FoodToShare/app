// ignore_for_file: prefer_const_constructors, avoid_print

import 'dart:convert';

import 'package:app/model/beneficiary_family_model.dart';
import 'package:app/model/beneficiary_model.dart';
import 'package:http/http.dart' as http;
import 'package:app/controller/globals.dart' as globals;

class BeneficiaryFamilyController {
  Future<List<BeneficiaryModel>> getBenefsFromPoint(int pointID) async {
    try {
      String token = globals.access_token.toString();
      var client = http.Client();
      var url = Uri.parse(globals.URI_API + '/api/v1/delivery/point/$pointID');
      var response = await client.get(
        url,
        headers: {
          "content-type": "application/json; charset=UTF-8",
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<BeneficiaryModel> beneficiaries = [];
        for (var i in json['beneficiaries']) {
          BeneficiaryModel beneficiary = BeneficiaryModel(
              i['id'],
              i['first_name'],
              i['last_name'],
              i['social_security'],
              i['cell_phone'],
              i['address'],
              i['delivery_point_id'],
              i['family_group']);
          beneficiaries.add(beneficiary);
        }
        return beneficiaries;
      } else {
        //return "Error";
        return List.empty();
      }
    } catch (e) {
      throw Exception("Failed to get beneficiaries.");
    }
  }

  Future deliveryComment(String comment, BeneficiaryFamilyModel family) async {
    try {
      for (var i in family.beneficiaries) {
        String token = globals.access_token.toString();

        var client = http.Client();
        var url = Uri.parse(globals.URI_API + '/api/v1/comment/delivery/');
        var response = await client.post(
          url,
          headers: {
            "content-type": "application/json; charset=UTF-8",
            "Accept": "application/json",
            "Authorization": "Bearer $token",
          },
          body: jsonEncode({
            "comments": comment,
            "beneficiaryID": i.id,
            "userID": globals.userID
          }),
        );

        if (response.statusCode == 200) {
          print("Success");
        } else {
          print("Error");
        }
      }
    } catch (e) {
      throw Exception("Failed to add comment.");
    }
  }
}
