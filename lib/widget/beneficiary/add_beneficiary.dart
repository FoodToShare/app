// ignore_for_file: prefer_const_constructors, avoid_print, prefer_const_literals_to_create_immutables, unused_import, sized_box_for_whitespace, unnecessary_const

import 'package:app/controller/points_controller.dart';
import 'package:app/model/points_model.dart';
import 'package:flutter/material.dart';
import 'package:app/view/login_page.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:app/controller/beneficiary_controller.dart';

MaterialColor mycolor = MaterialColor(
  0xFFE67E22,
  <int, Color>{
    50: Color(0xFFE67E22),
    100: Color(0xFFE67E22),
    200: Color(0xFFE67E22),
    300: Color(0xFFE67E22),
    400: Color(0xFFE67E22),
    500: Color(0xFFE67E22),
    600: Color(0xFFE67E22),
    700: Color(0xFFE67E22),
    800: Color(0xFFE67E22),
    900: Color(0xFFE67E22),
  },
);

class AddBeneficiario extends StatefulWidget {
  const AddBeneficiario({Key? key}) : super(key: key);

  @override
  _AddBeneficiarioState createState() => _AddBeneficiarioState();
}

class _AddBeneficiarioState extends State<AddBeneficiario> {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        _onAlertWithCustomContentPressed(context);
      },
      child: Icon(FontAwesomeIcons.userPlus),
      backgroundColor: mycolor,
    );
  }

  _onAlertWithCustomContentPressed(context) async {
    String firstName = '';
    String lastName = '';
    String address = '';
    String cellphone = '';
    String socialSecurity = '';
    String idPoint = '0';

    Alert(
        context: context,
        title: "Create beneficiary",
        content: Column(
          children: <Widget>[
            SizedBox(height: 25),
            TextField(
              onChanged: (text) {
                firstName = text;
              },
              decoration: InputDecoration(
                labelText: 'First name',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 5),
            TextField(
              onChanged: (text) {
                lastName = text;
              },
              decoration: InputDecoration(
                labelText: 'Last Name',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 5),
            TextField(
              onChanged: (text) {
                socialSecurity = text;
              },
              keyboardType: TextInputType.number,
              maxLength: 11,
              decoration: InputDecoration(
                labelText: 'Social Security number',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 5),
            TextField(
              onChanged: (text) {
                cellphone = text;
              },
              keyboardType: TextInputType.number,
              maxLength: 9,
              decoration: InputDecoration(
                labelText: 'Cellphone',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 5),
            TextField(
              onChanged: (text) {
                address = text;
              },
              decoration: InputDecoration(
                labelText: 'Address',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 5),
            Container(
              height: 53.0,
              child: DropdownButtonFormField<String>(
                items: await getPoints(),
                value: idPoint,
                isExpanded: true,
                onChanged: (value) => setState(() {
                  idPoint = value!;
                }),
                hint: Text("Select point"),
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 30.0,
                    color: Colors.black),
                decoration: InputDecoration(
                  labelText: "User",
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(5.0),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        buttons: [
          DialogButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              if (firstName == "" ||
                  lastName == "" ||
                  socialSecurity == "" ||
                  cellphone == "" ||
                  address == "") {
                showToast("All fields are mandatory");
              } else {
                createBeneficiary(firstName, lastName, socialSecurity,
                    cellphone, address, idPoint);
              }
            },
            child: Text(
              "Create",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  createBeneficiary(
      firstName, lastName, socialSecurity, cellphone, address, idPoint) async {
    String response = await BeneficiaryController().createBeneficiary(firstName,
        lastName, socialSecurity, cellphone, address, int.tryParse(idPoint)!);
    if (response == "success") {
      showToast("Beneficiary created successfully");
      Navigator.of(context).pushReplacementNamed('/beneficiary');
    } else {
      showToast("Error creating beneficiary");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  getPoints() async {
    List<PointsModel> points = <PointsModel>[];
    await PointController().getAllPoints().then((value) => {
          setState(() {
            points.addAll(value);
          })
        });

    List<DropdownMenuItem<String>> dropPoint = <DropdownMenuItem<String>>[];
    dropPoint.add(DropdownMenuItem(
      value: "0",
      child: Text(
        "Dont associate to any point",
        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
      ),
    ));

    for (var i in points) {
      dropPoint.add(DropdownMenuItem(
        value: i.id.toString(),
        child: Text(
          i.name,
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
        ),
      ));
    }
    return dropPoint;
  }
}
