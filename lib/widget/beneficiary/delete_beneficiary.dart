// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unnecessary_this, no_logic_in_create_state

import 'package:app/controller/beneficiary_controller.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class DeleteBeneficiary extends StatelessWidget {
  const DeleteBeneficiary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  modalDeleteBeneficiary(context, idBeneficiary) {
    return Alert(
        title: "Delete beneficiary",
        context: context,
        desc: "Are you sure you want to remove this beneficiary?",
        buttons: [
          DialogButton(
              onPressed: () async {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.green),
          DialogButton(
            onPressed: () async {
              deleteBeneficiary(context, idBeneficiary);
            },
            child: Text(
              "Delete",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.red,
          )
        ]).show();
  }

  deleteBeneficiary(context, idBeneficiary) async {
    String response =
        await BeneficiaryController().deleteBeneficiary(idBeneficiary);
    if (response == "success") {
      showToast("User deleted beneficiary");
      Navigator.of(context).pushReplacementNamed('/beneficiary');
    } else {
      showToast("Error  when deleting beneficiary");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
