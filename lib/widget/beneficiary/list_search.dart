// ignore_for_file: avoid_print, unused_import, prefer_const_constructors, prefer_final_fields

import 'package:app/controller/beneficiary_controller.dart';
import 'package:app/model/beneficiary_model.dart';
import 'package:app/widget/beneficiary/delete_beneficiary.dart';
import 'package:app/widget/beneficiary/edit_beneficiary.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListSearch extends StatefulWidget {
  const ListSearch({Key? key}) : super(key: key);

  @override
  _ListSearchState createState() => _ListSearchState();
}

class _ListSearchState extends State<ListSearch> {
  List<BeneficiaryModel> _beneficiaries = <BeneficiaryModel>[];
  List<BeneficiaryModel> _beneficiariesForDisplay = <BeneficiaryModel>[];

  @override
  void initState() {
    BeneficiaryController().getBenefs().then((value) => {
          setState(() {
            _beneficiaries.addAll(value);
            _beneficiariesForDisplay = _beneficiaries;
          })
        });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemCount: _beneficiariesForDisplay.length + 1,
          itemBuilder: (context, index) {
            return index == 0 ? _searchBar() : _listItem(index - 1);
          }),
    );
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration:
            InputDecoration(hintText: "Search ...", icon: Icon(Icons.search)),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            _beneficiariesForDisplay = _beneficiaries.where((beneficiary) {
              var fullName = beneficiary.firstName + " " + beneficiary.lastName;
              var beneficiaryTitle = fullName.toLowerCase();
              return beneficiaryTitle.contains(text);
            }).toList();
          });
        },
      ),
    );
  }

  _listItem(index) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.grey, width: 0.8),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
            top: 0.0, bottom: 0.0, left: 16.0, right: 0.0),
        child: Row(
          // ignore: prefer_const_literals_to_create_immutables
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 16.0, bottom: 16.0, left: 0.0, right: 0.0),
              child: Column(
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  Text.rich(TextSpan(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      WidgetSpan(
                          child: Icon(
                        FontAwesomeIcons.userAlt,
                        color: Colors.black,
                        size: 35,
                      ))
                    ],
                  )),
                ],
              ),
            ),
            Column(
              children: [
                Text(
                  _beneficiariesForDisplay[index].firstName +
                      " " +
                      _beneficiariesForDisplay[index].lastName,
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 4.0),
                Text(
                  _beneficiariesForDisplay[index].address,
                  style: TextStyle(color: Colors.grey.shade600),
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                buttonsList(context, _beneficiariesForDisplay[index]),
              ],
            )
          ],
        ),
      ),
    );
  }

  buttonsList(context, beneficiary) {
    return Column(
      children: [
        Row(
          children: [
            Material(
              color: Color.fromRGBO(225, 230, 231, 1.0),
              borderRadius: BorderRadius.circular(0.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 50.0,
                  child: MaterialButton(
                    onPressed: () async {
                      await EditBeneficiary()
                          .createState()
                          .modalEditBeneficiary(context, beneficiary);
                    },
                    child: InkWell(
                      child: Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.userEdit,
                            color: Color.fromRGBO(44, 62, 80, 1.0),
                            size: 25,
                          ))
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 1.0,
            ),
            Material(
              color: Color.fromRGBO(225, 230, 231, 1.0),
              borderRadius: BorderRadius.circular(0.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 50.0,
                  child: MaterialButton(
                    onPressed: () async {
                      await DeleteBeneficiary()
                          .modalDeleteBeneficiary(context, beneficiary.id);
                    },
                    child: InkWell(
                      child: Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.userMinus,
                            color: Color.fromRGBO(44, 62, 80, 1.0),
                            size: 25,
                          ))
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
