// ignore_for_file: unused_element, unnecessary_new, prefer_const_constructors

import 'package:app/controller/points_controller.dart';
import 'package:app/controller/routes_controller.dart';
import 'package:app/model/routes_model.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:location/location.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class UpdatePoints extends StatefulWidget {
  const UpdatePoints({Key? key}) : super(key: key);

  @override
  _UpdatePointsState createState() => _UpdatePointsState();
}

class _UpdatePointsState extends State<UpdatePoints> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }

  modalUpdateUser(context, point) async {
    String lat = point.latitude.toString();
    String long = point.longitude.toString();
    String name = point.name;
    String description = point.description;
    String route = point.routId.toString();
    var latV = new TextEditingController(text: point.latitude.toString());
    var lngV = new TextEditingController(text: point.longitude.toString());
    var nameV = new TextEditingController(text: point.name);
    var descriptionV = new TextEditingController(text: point.description);
    List<DropdownMenuItem<String>> dropPoint = <DropdownMenuItem<String>>[];
    dropPoint = await getRoutes();
    bool haveId = false;
    for (var i in dropPoint) {
      if (i.value.toString() == route) {
        haveId = true;
      }
    }
    if (!haveId) {
      route = "0";
    }
    Alert(
        context: context,
        title: "Create Point",
        content: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          child: Column(
            children: [
              TextField(
                controller: nameV,
                onChanged: (text) {
                  name = text;
                },
                decoration: InputDecoration(
                  labelText: 'Name',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 5.0),
              TextField(
                controller: descriptionV,
                onChanged: (text) {
                  description = text;
                },
                decoration: InputDecoration(
                  labelText: 'Description',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 7.0),
              DropdownButtonFormField<String>(
                items: dropPoint,
                value: route,
                isExpanded: true,
                onChanged: (value) => setState(() {
                  route = value!;
                }),
                hint: Text("Select point"),
              ),
              SizedBox(height: 5.0),
              SizedBox(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextField(
                      controller: latV,
                      onChanged: (text) {
                        lat = text;
                      },
                      decoration: InputDecoration(
                        labelText: 'Latitude',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    TextField(
                      controller: lngV,
                      onChanged: (text) {
                        long = text;
                      },
                      decoration: InputDecoration(
                        labelText: 'Longitude',
                        border: OutlineInputBorder(),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 5.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Material(
                    color: Color.fromRGBO(225, 230, 231, 1.0),
                    borderRadius: BorderRadius.circular(5.0),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        width: 300.0,
                        height: 20.0,
                        child: MaterialButton(
                          onPressed: () async {
                            var currentLocation = await getLocation();
                            setState(() {
                              lat = currentLocation.latitude.toString();
                              long = currentLocation.longitude.toString();
                              latV.text = currentLocation.latitude.toString();
                              lngV.text = currentLocation.longitude.toString();
                            });
                          },
                          child: InkWell(
                            child: Text.rich(TextSpan(
                              // ignore: prefer_const_literals_to_create_immutables
                              children: [
                                WidgetSpan(
                                    child: Icon(
                                  FontAwesomeIcons.compass,
                                  color: Color.fromRGBO(44, 62, 80, 1.0),
                                  size: 20,
                                ))
                              ],
                            )),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        buttons: [
          DialogButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              if (lat == "" || long == "" || description == "" || name == "") {
                showToast("All fields are mandatory");
              } else {
                updatePoint(
                    context, name, description, route, lat, long, point.id);
              }
            },
            child: Text(
              "Create",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  updatePoint(context, name, description, idRout, lat, long, idPoint) async {
    String response = await PointController()
        .updatePoint(name, description, idRout, lat, long, idPoint.toString());
    if (response == "success") {
      showToast("Point edited successfully");
      Navigator.of(context).pushReplacementNamed('/point');
    } else {
      showToast("Error editing point");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  getRoutes() async {
    List<RoutesModel> routes = <RoutesModel>[];
    await RouteController().getRoutes().then((value) => {routes.addAll(value)});

    List<DropdownMenuItem<String>> dropPoint = <DropdownMenuItem<String>>[];
    dropPoint.add(DropdownMenuItem(
      value: "0",
      child: Text(
        "Dont associate to any point",
        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
      ),
    ));

    for (var i in routes) {
      dropPoint.add(DropdownMenuItem(
        value: i.id.toString(),
        child: Text(
          i.name,
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
        ),
      ));
    }
    return dropPoint;
  }

  getLocation() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _locationData = await location.getLocation();
    return _locationData;
  }
}
