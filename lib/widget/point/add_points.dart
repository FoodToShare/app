// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, library_prefixes, prefer_typing_uninitialized_variables, unused_local_variable, avoid_unnecessary_containers, unnecessary_new, duplicate_ignore, unused_import, sized_box_for_whitespace, unnecessary_const

import 'package:app/controller/points_controller.dart';
import 'package:app/controller/routes_controller.dart';
import 'package:app/controller/user_controller.dart';
import 'package:app/model/routes_model.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart' as latLng;
import 'package:location/location.dart';

MaterialColor mycolor = MaterialColor(
  0xFFE67E22,
  <int, Color>{
    50: Color(0xFFE67E22),
    100: Color(0xFFE67E22),
    200: Color(0xFFE67E22),
    300: Color(0xFFE67E22),
    400: Color(0xFFE67E22),
    500: Color(0xFFE67E22),
    600: Color(0xFFE67E22),
    700: Color(0xFFE67E22),
    800: Color(0xFFE67E22),
    900: Color(0xFFE67E22),
  },
);
String lat = "";
String long = "";
var mapa;
List<Marker> markers = <Marker>[];

class AddPoint extends StatefulWidget {
  const AddPoint({Key? key}) : super(key: key);

  @override
  _AddPointState createState() => _AddPointState();
}

class _AddPointState extends State<AddPoint> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        _modalCreateUser(context);
      },
      child: Icon(FontAwesomeIcons.plus),
      backgroundColor: mycolor,
    );
  }

  _modalCreateUser(context) async {
    String name = "";
    String description = "";
    String route = "0";
    List<latLng.LatLng> marks = [];
    var latV = new TextEditingController(text: "");
    var lngV = new TextEditingController(text: "");
    Alert(
        context: context,
        title: "Create Point",
        content: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          child: Column(
            children: [
              TextField(
                onChanged: (text) {
                  name = text;
                },
                decoration: InputDecoration(
                  labelText: 'Name',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 5.0),
              TextField(
                onChanged: (text) {
                  description = text;
                },
                decoration: InputDecoration(
                  labelText: 'Description',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 7.0),
              Container(
                height: 53.0,
                child: DropdownButtonFormField<String>(
                  items: await getRoutes(),
                  value: route,
                  isExpanded: true,
                  onChanged: (value) => setState(() {
                    route = value!;
                  }),
                  hint: Text("Select point"),
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 30.0,
                      color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "Select point",
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              SizedBox(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextField(
                      controller: latV,
                      onChanged: (text) {
                        lat = text;
                      },
                      decoration: InputDecoration(
                        labelText: 'Latitude',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    TextField(
                      controller: lngV,
                      onChanged: (text) {
                        long = text;
                      },
                      decoration: InputDecoration(
                        labelText: 'Longitude',
                        border: OutlineInputBorder(),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 5.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Material(
                    color: Color.fromRGBO(225, 230, 231, 1.0),
                    borderRadius: BorderRadius.circular(5.0),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        width: 300.0,
                        height: 20.0,
                        child: MaterialButton(
                          onPressed: () async {
                            var currentLocation = await getLocation();
                            setState(() {
                              lat = currentLocation.latitude.toString();
                              long = currentLocation.longitude.toString();
                              latV.text = currentLocation.latitude.toString();
                              lngV.text = currentLocation.longitude.toString();
                            });
                          },
                          child: InkWell(
                            child: Text.rich(TextSpan(
                              // ignore: prefer_const_literals_to_create_immutables
                              children: [
                                WidgetSpan(
                                    child: Icon(
                                  FontAwesomeIcons.compass,
                                  color: Color.fromRGBO(44, 62, 80, 1.0),
                                  size: 20,
                                ))
                              ],
                            )),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        buttons: [
          DialogButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              if (lat == "" || long == "" || description == "" || name == "") {
                showToast("All fields are mandatory");
              } else {
                createPoint(name, description, route, lat, long);
              }
            },
            child: Text(
              "Create",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  createPoint(name, description, idRout, lat, long) async {
    String response = await PointController()
        .createPoint(name, description, idRout, lat, long);
    if (response == "success") {
      showToast("Point created successfully");
      Navigator.of(context).pushReplacementNamed('/point');
    } else {
      showToast("Error creating point");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  getRoutes() async {
    List<RoutesModel> routes = <RoutesModel>[];
    await RouteController().getRoutes().then((value) => {
          setState(() {
            routes.addAll(value);
          })
        });

    List<DropdownMenuItem<String>> dropPoint = <DropdownMenuItem<String>>[];
    dropPoint.add(DropdownMenuItem(
      value: "0",
      child: Text(
        "Dont associate to any point",
        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
      ),
    ));

    for (var i in routes) {
      dropPoint.add(DropdownMenuItem(
        value: i.id.toString(),
        child: Text(
          i.name,
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
        ),
      ));
    }
    return dropPoint;
  }

  getLocation() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _locationData = await location.getLocation();
    return _locationData;
  }
}

/*
Container(
                height: MediaQuery.of(context).size.width / 1.5,
                width: MediaQuery.of(context).size.width / 1.5,
                child: FlutterMap(
                  options: MapOptions(
                    rotationWinGestures: MultiFingerGesture.none,
                    rotation: 0,
                    center: latLng.LatLng(41.693685, -8.846804),
                    zoom: 13.0,
                    minZoom: 0.0,
                    onTap: (tapPosition, point) {
                      setState(() {
                        createMarker(point);
                      });
                    },
                  ),
                  layers: [
                    TileLayerOptions(
                      urlTemplate:
                          "https://api.mapbox.com/styles/v1/leonardolopez/ckz4nnhvh00g015ldyxpgaxzh/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGVvbmFyZG9sb3BleiIsImEiOiJja3o0bmlmZXgwZnprMndtZWx1MWlnc2xkIn0.o6yPemAOxi90BX5uX5C98w",
                      additionalOptions: {
                        'accessToken':
                            'pk.eyJ1IjoibGVvbmFyZG9sb3BleiIsImEiOiJja3o0bmlmZXgwZnprMndtZWx1MWlnc2xkIn0.o6yPemAOxi90BX5uX5C98w',
                        'id': 'mapbox.satellite',
                      },
                    ),
                    MarkerLayerOptions(markers: markers),
                  ],
                ),
              ),
               createMarker(point) {
    markers.clear();
    try {
      var marker = Marker(
        width: 50.0,
        height: 50.0,
        point: latLng.LatLng(point.latitude, point.longitude),
        builder: (ctx) => Container(
          child: Icon(FontAwesomeIcons.mapMarker),
        ),
      );
      markers.add(marker);
    } catch (e) {
      throw Exception("FUDEU");
    }

    /*
  setState(() {
      markers = <Marker>[
        Marker(
          width: 50.0,
          height: 50.0,
          point: latLng.LatLng(point.latitude, point.longitude),
          builder: (ctx) => Container(
            key: Key('blue'),
            child: Icon(FontAwesomeIcons.mapMarker),
          ),
        )
      ];
    });
  */
  }
 */
