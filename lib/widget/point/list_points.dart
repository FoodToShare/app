// ignore_for_file: prefer_const_constructors, library_prefixes, camel_case_types, type_init_formals

import 'package:app/app_widget.dart';
import 'package:app/controller/points_controller.dart';
import 'package:app/model/points_model.dart';
import 'package:app/widget/point/delete_points.dart';
import 'package:app/widget/point/update_points.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:latlong2/latlong.dart' as latLng;

List _points = <PointsModel>[];
bool pointList = true;
List<Marker> markers = <Marker>[];

class ListPoint extends StatefulWidget {
  const ListPoint({Key? key}) : super(key: key);

  @override
  _ListPointState createState() => _ListPointState();
}

class _ListPointState extends State<ListPoint> {
  final _pageController = PageController();

  @override
  void initState() {
    _points.clear();
    PointController().getAllPoints().then((value) => {
          setState(() {
            _points.addAll(value);
          })
        });
    prepareMarkers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    !pointList ? prepareMarkers() : "";
    return pointList ? createList() : createMap();
  }

  changePageView() {
    pointList = !pointList;
  }

  prepareMarkers() {
    markers.clear();
    for (var i in _points) {
      markers.add(Marker(
          point: latLng.LatLng(i.latitude, i.longitude),
          builder: (_) {
            return GestureDetector(
              onTap: () {
                _pageController.animateToPage(i.id,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.bounceInOut);
              },
              child: _myMarker(),
            );
          }));
    }
  }

  _myMarker() {
    return Container(
      height: 15,
      width: 15,
      decoration: BoxDecoration(
          color: mycolor,
          shape: BoxShape.circle,
          border: Border.all(color: Color.fromRGBO(44, 62, 80, 1.0))),
    );
  }

  createList() {
    return Scaffold(
        body: Padding(
      padding:
          const EdgeInsets.only(top: 16.0, bottom: 0.0, left: 0.0, right: 0.0),
      child: ListView.builder(
          itemCount: _points.length,
          itemBuilder: (context, index) {
            return _listPoint(index, context, _points[index]);
          }),
    ));
  }

  createMap() {
    return Stack(
      children: [
        FlutterMap(
          options: MapOptions(
            rotationWinGestures: MultiFingerGesture.none,
            rotation: 0,
            center: latLng.LatLng(41.693685, -8.846804),
            zoom: 7.0,
            minZoom: 0.0,
            onTap: (tapPosition, point) {},
          ),
          nonRotatedLayers: [
            TileLayerOptions(
              urlTemplate:
                  "https://api.mapbox.com/styles/v1/leonardolopez/ckz4nnhvh00g015ldyxpgaxzh/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGVvbmFyZG9sb3BleiIsImEiOiJja3o0bmlmZXgwZnprMndtZWx1MWlnc2xkIn0.o6yPemAOxi90BX5uX5C98w",
              additionalOptions: {
                'accessToken':
                    'pk.eyJ1IjoibGVvbmFyZG9sb3BleiIsImEiOiJja3o0bmlmZXgwZnprMndtZWx1MWlnc2xkIn0.o6yPemAOxi90BX5uX5C98w',
                'id': 'mapbox.satellite',
              },
            ),
            MarkerLayerOptions(markers: markers)
          ],
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 20,
          height: MediaQuery.of(context).size.height * 0.25,
          child: PageView.builder(
              controller: _pageController,
              itemCount: markers.length,
              itemBuilder: (context, index) {
                final item = _points[index];
                return _mapItemDetails(
                  point: item,
                );
              }),
        ),
      ],
    );
  }

  _listPoint(index, context, point) {
    var widthCard = MediaQuery.of(context).size.width / 2.3;
    return InkWell(
        onTap: () {},
        child: Card(
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.grey, width: 0.8),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                top: 0.0, bottom: 0.0, left: 16.0, right: 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16.0, bottom: 16.0, left: 0.0, right: 0.0),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.mapMarkerAlt,
                            color: Colors.black,
                            size: 35,
                          ))
                        ],
                      )),
                    ],
                  ),
                ),
                SizedBox(
                  width: widthCard,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(point.name),
                      Text(
                        point.description,
                        style: TextStyle(color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    buttonsList(context, point),
                  ],
                )
              ],
            ),
          ),
        ));
  }

  buttonsList(context, point) {
    return Column(
      children: [
        Row(
          children: [
            Material(
              color: Color.fromRGBO(225, 230, 231, 1.0),
              borderRadius: BorderRadius.circular(0.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 50.0,
                  child: MaterialButton(
                    onPressed: () async {
                      await UpdatePoints()
                          .createState()
                          .modalUpdateUser(context, point);
                    },
                    child: InkWell(
                      child: Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.pencilAlt,
                            color: Color.fromRGBO(44, 62, 80, 1.0),
                            size: 25,
                          ))
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 1.0,
            ),
            Material(
              color: Color.fromRGBO(225, 230, 231, 1.0),
              borderRadius: BorderRadius.circular(0.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 50.0,
                  child: MaterialButton(
                    onPressed: () async {
                      await DeletePoint().modalDeletePoint(context, point.id);
                    },
                    child: InkWell(
                      child: Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.trash,
                            color: Color.fromRGBO(44, 62, 80, 1.0),
                            size: 25,
                          ))
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class _mapItemDetails extends StatelessWidget {
  final PointsModel point;
  const _mapItemDetails({
    Key? key,
    required PointsModel this.point,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var widthCard = MediaQuery.of(context).size.width / 1.5;
    return Padding(
        padding: const EdgeInsets.all(15.0),
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(
                top: 16.0, bottom: 0.0, left: 16.0, right: 16.0),
            child: Column(
              children: [
                Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        Text.rich(TextSpan(
                          // ignore: prefer_const_literals_to_create_immutables
                          children: [
                            WidgetSpan(
                                child: Icon(
                              FontAwesomeIcons.mapMarkerAlt,
                              color: Color.fromRGBO(44, 62, 80, 1.0),
                              size: 35,
                            ))
                          ],
                        )),
                      ],
                    ),
                    SizedBox(width: 15.0),
                    SizedBox(
                      width: widthCard,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            point.name,
                            style: TextStyle(fontSize: 25.0),
                          ),
                          Text(
                            point.description,
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Center(
                  child: Row(
                    children: [
                      Material(
                        color: Color.fromRGBO(225, 230, 231, 1.0),
                        borderRadius: BorderRadius.circular(0.0),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.35,
                            height: 40,
                            child: MaterialButton(
                              onPressed: () async {
                                await UpdatePoints()
                                    .createState()
                                    .modalUpdateUser(context, point);
                              },
                              child: InkWell(
                                child: Text.rich(TextSpan(
                                  // ignore: prefer_const_literals_to_create_immutables
                                  children: [
                                    WidgetSpan(
                                        child: Icon(
                                      FontAwesomeIcons.pencilAlt,
                                      color: Color.fromRGBO(44, 62, 80, 1.0),
                                      size: 25,
                                    ))
                                  ],
                                )),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Material(
                        color: Color.fromRGBO(225, 230, 231, 1.0),
                        borderRadius: BorderRadius.circular(0.0),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.35,
                            height: 40,
                            child: MaterialButton(
                              onPressed: () async {
                                await DeletePoint()
                                    .modalDeletePoint(context, point.id);
                              },
                              child: InkWell(
                                child: Text.rich(TextSpan(
                                  // ignore: prefer_const_literals_to_create_immutables
                                  children: [
                                    WidgetSpan(
                                        child: Icon(
                                      FontAwesomeIcons.trash,
                                      color: Color.fromRGBO(44, 62, 80, 1.0),
                                      size: 25,
                                    ))
                                  ],
                                )),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
