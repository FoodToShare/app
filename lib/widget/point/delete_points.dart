// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unnecessary_this, no_logic_in_create_state, unused_import

import 'package:app/controller/points_controller.dart';
import 'package:app/controller/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class DeletePoint extends StatelessWidget {
  const DeletePoint({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  modalDeletePoint(context, pointID) {
    return Alert(
        title: "Delete point",
        context: context,
        desc: "Are you sure you want to remove this point?",
        buttons: [
          DialogButton(
              onPressed: () async {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.green),
          DialogButton(
            onPressed: () async {
              deletePoint(pointID, context);
            },
            child: Text(
              "Delete",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.red,
          )
        ]).show();
  }

  deletePoint(pointID, context) async {
    String response = await PointController().deletePoint(pointID);
    if (response == "success") {
      showToast("Point deleted successfully");
      Navigator.of(context).pushReplacementNamed('/point');
    } else {
      showToast("Error when deleting point");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
