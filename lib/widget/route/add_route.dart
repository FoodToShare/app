// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_final_fields, unnecessary_const, sized_box_for_whitespace

import 'package:app/controller/routes_controller.dart';
import 'package:app/controller/user_controller.dart';
import 'package:app/model/users_model.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

MaterialColor mycolor = MaterialColor(
  0xFFE67E22,
  <int, Color>{
    50: Color(0xFFE67E22),
    100: Color(0xFFE67E22),
    200: Color(0xFFE67E22),
    300: Color(0xFFE67E22),
    400: Color(0xFFE67E22),
    500: Color(0xFFE67E22),
    600: Color(0xFFE67E22),
    700: Color(0xFFE67E22),
    800: Color(0xFFE67E22),
    900: Color(0xFFE67E22),
  },
);
String nameRoute = "";
String descRoute = "";
String userIdRoute = "";
List<UserModel> _users = <UserModel>[];

class AddRoute extends StatefulWidget {
  const AddRoute({Key? key}) : super(key: key);

  @override
  _AddRouteState createState() => _AddRouteState();
}

class _AddRouteState extends State<AddRoute> {
  String _valueUser = "0";
  @override
  void initState() {
    _users.clear();
    UserController().getUsers().then((value) => {
          setState(() {
            _users.addAll(value);
          })
        });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        _onAlertWithCustomContentPressed(context);
      },
      child: const Icon(Icons.add),
      backgroundColor: mycolor,
    );
  }

  _onAlertWithCustomContentPressed(context) {
    Alert(
        context: context,
        title: "Create route",
        content: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          child: Column(
            children: [
              SizedBox(
                height: 10.0,
              ),
              TextField(
                style: TextStyle(fontSize: 15.0, height: 0.5),
                decoration: InputDecoration(
                  labelText: "Route name",
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(5.0),
                    ),
                  ),
                ),
                onChanged: (value) {
                  nameRoute = value;
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              //----------------------------
              TextField(
                style: TextStyle(fontSize: 15.0, height: 0.5),
                decoration: InputDecoration(
                  labelText: "Route description",
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(5.0),
                    ),
                  ),
                ),
                onChanged: (value) {
                  descRoute = value;
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              //----------------------------
              Container(
                height: 53.0,
                child: DropdownButtonFormField<String>(
                  items: prepareData(),
                  value: _valueUser,
                  isExpanded: true,
                  onChanged: (value) => setState(() {
                    _valueUser = value!;
                  }),
                  hint: Text("Select user"),
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 30.0,
                      color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "User",
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        buttons: [
          DialogButton(
              onPressed: () {
                nameRoute = "";
                descRoute = "";
                userIdRoute = "";
                _valueUser = "0";
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              await createRoute(context);
              Navigator.of(context).pushReplacementNamed('/routes');
            },
            child: Text(
              "Create",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  createRoute(context) async {
    if (nameRoute != "" && descRoute != "" && _valueUser != "") {
      RouteController()
          .createRoute(nameRoute, descRoute, int.tryParse(_valueUser)!);
      Navigator.pop(context);
    } else {
      showToast("All fields are mandatory");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  prepareData() {
    List<DropdownMenuItem<String>> items = <DropdownMenuItem<String>>[];
    items.add(DropdownMenuItem(
      value: "0",
      child: Text(
        "Dont associate any user.",
        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
      ),
    ));
    for (var i in _users) {
      items.add(DropdownMenuItem(
        value: i.id.toString(),
        child: Text(
          i.email,
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
        ),
      ));
    }
    return items;
  }
}
