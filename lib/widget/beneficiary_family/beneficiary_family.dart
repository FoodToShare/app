// ignore_for_file: prefer_const_constructors, avoid_print, prefer_final_fields, unused_element, no_logic_in_create_state, unnecessary_this

import 'package:app/model/beneficiary_family_model.dart';
import 'package:app/model/beneficiary_model.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:app/controller/beneficiary_family_controller.dart';

class BeneficiaryFamily extends StatefulWidget {
  final int point;
  const BeneficiaryFamily(this.point, {Key? key}) : super(key: key);

  @override
  _BeneficiaryFamilyState createState() =>
      _BeneficiaryFamilyState(pointID: this.point);
}

class _BeneficiaryFamilyState extends State<BeneficiaryFamily> {
  List<BeneficiaryModel> _beneficiaries = <BeneficiaryModel>[];
  List<BeneficiaryFamilyModel> families = <BeneficiaryFamilyModel>[];
  bool isChecked = false;
  int pointID;
  _BeneficiaryFamilyState({required this.pointID});

  @override
  void initState() {
    BeneficiaryFamilyController()
        .getBenefsFromPoint(this.pointID)
        .then((value) => {
              setState(() {
                _beneficiaries.addAll(value);
                families = agroupByFamily(value);
              }),
            });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (families.isNotEmpty) {
      //return createWidget(families, context);
      return Scaffold(
          body: Padding(
        padding: const EdgeInsets.only(
            top: 16.0, bottom: 0.0, left: 0.0, right: 0.0),
        child: Column(
          children: createWidget(families, context),
        ),
      ));
    } else {
      //return Center(child: CircularProgressIndicator());
      return Center(
        child: Text("Dont have beneficiaries associated to this point!"),
      );
    }
  }

  createWidget(families, context) {
    List<Widget> widget = [];
    for (var i in families) {
      widget.add(InkWell(
          onTap: () {
            setState(() {
              if (!i.received) {
                isChecked = !isChecked;
                i.received = !i.received;
                _onDeliveryLunchbox(context, i);
              }
            });
          },
          child: Card(
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.grey, width: 0.8),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 18.0, bottom: 18.0, left: 16.0, right: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // ignore: prefer_const_literals_to_create_immutables
                      children: listBeneficiaries(i.beneficiaries)),
                  Column(
                    children: [
                      Row(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          Text.rich(TextSpan(
                            // ignore: prefer_const_literals_to_create_immutables
                            children: [
                              WidgetSpan(
                                  child: Icon(
                                FontAwesomeIcons.dolly,
                                color: i.received
                                    ? Colors.green
                                    : Colors.grey.shade300,
                                size: 25,
                              ))
                            ],
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Row(
                        children: [
                          i.received
                              ? Text(
                                  "Delivered",
                                  style: TextStyle(
                                      color: Colors.green, fontSize: 12),
                                )
                              : Text(
                                  "to be delivered",
                                  style: TextStyle(
                                      color: Colors.grey.shade400,
                                      fontSize: 12),
                                )
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          )));
    }

    return widget;
  }

  _onDeliveryLunchbox(context, family) {
    var alertStyle = AlertStyle(
      descStyle: TextStyle(fontWeight: FontWeight.normal),
      titleStyle: TextStyle(
          color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
      alertElevation: 0,
    );

    Alert(
      title: "Lunchbox",
      style: alertStyle,
      desc: "The beneficiary have returned some lunchbox?",
      // ignore: prefer_const_literals_to_create_immutables
      buttons: [
        DialogButton(
            onPressed: () async {
              Navigator.pop(context);
              _onAlertAskForComment(context, family);
            },
            child: Text(
              "No",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.red),
        DialogButton(
          onPressed: () async {
            Navigator.pop(context);
            _onAlertAskForComment(context, family);
          },
          child: Text(
            "Yes",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          height: 35.0,
          color: Colors.green,
        )
      ],
      context: context,
    ).show();
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  agroupByFamily(beneficiaries) {
    //beneficiaries.sort((a, b) => a["familyGroup"].compareTo(b["familyGroup"]));
    try {
      List<BeneficiaryFamilyModel> groups = <BeneficiaryFamilyModel>[];
      for (var i in beneficiaries) {
        if (groups.isEmpty) {
          List<BeneficiaryModel> beneficiary = <BeneficiaryModel>[];
          beneficiary.add(i);
          BeneficiaryFamilyModel benef =
              BeneficiaryFamilyModel(i.familyGroup, beneficiary, false);
          groups.add(benef);
        } else {
          var hasFammily = false;
          for (var j in groups) {
            if (j.familyGroup == i.familyGroup) {
              j.beneficiaries.add(i);
              hasFammily = true;
            }
          }
          if (!hasFammily) {
            List<BeneficiaryModel> beneficiary = <BeneficiaryModel>[];
            beneficiary.add(i);
            BeneficiaryFamilyModel benef =
                BeneficiaryFamilyModel(i.familyGroup, beneficiary, false);
            groups.add(benef);
          }
        }
      }
      return groups;
    } catch (e) {
      rethrow;
    }
  }

  listBeneficiaries(beneficiaries) {
    List<Widget> widgets = [];
    for (var j in beneficiaries) {
      String name = j.firstName + " " + j.lastName;
      var teste = Text(
        name,
        style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
      );
      widgets.add(teste);
    }
    return widgets;
  }
}

_onAlertAskForComment(context, family) {
  var alertStyle = AlertStyle(
    descStyle: TextStyle(fontWeight: FontWeight.normal),
    titleStyle: TextStyle(
        color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25.0),
    alertElevation: 0,
  );

  Alert(
      context: context,
      title: "Comment about the delivery",
      style: alertStyle,
      desc: "Did you have a problem with this delivery?",
      buttons: [
        DialogButton(
            onPressed: () async {
              Navigator.pop(context);
            },
            child: Text(
              "No",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.red),
        DialogButton(
          onPressed: () async {
            Navigator.pop(context);
            _onAlertPutComment(context, family);
          },
          child: Text(
            "Yes",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          height: 35.0,
          color: Colors.green,
        )
      ]).show();
}

_onAlertPutComment(context, family) {
  var alertStyle = AlertStyle(
    descStyle: TextStyle(fontWeight: FontWeight.normal),
    titleStyle: TextStyle(
        color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25.0),
    alertElevation: 0,
  );
  String comment = "";
  Alert(
      context: context,
      title: "Comment about the delivery",
      style: alertStyle,
      desc: "",
      content: SizedBox(
        height: 90,
        child: TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Put your commentary here!',
          ),
          expands: true,
          maxLines: null,
          onChanged: (value) {
            comment = value;
          },
        ),
      ),
      buttons: [
        DialogButton(
            onPressed: () async {
              Navigator.pop(context);
              _onAlertAskForComment(context, family);
            },
            child: Text(
              "Cancel",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.red),
        DialogButton(
          onPressed: () async {
            Navigator.pop(context);
            BeneficiaryFamilyController().deliveryComment(comment, family);
          },
          child: Text(
            "Save",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          height: 35.0,
          color: Colors.green,
        )
      ]).show();
}
