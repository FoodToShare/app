// ignore_for_file: prefer_const_constructors, avoid_print

import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ReturnLunchbox extends StatefulWidget {
  const ReturnLunchbox({Key? key}) : super(key: key);
  @override
  _ReturnLunchboxState createState() => _ReturnLunchboxState();
}

class _ReturnLunchboxState extends State<ReturnLunchbox> {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        _onDeliveryLunchbox(context);
      },
      child: const Icon(Icons.add),
      backgroundColor: Colors.blue,
    );
  }
}

_onDeliveryLunchbox(context) {
  var alertStyle = AlertStyle(
    descStyle: TextStyle(fontWeight: FontWeight.normal),
    titleStyle: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
    ),
    alertElevation: 0,
  );

  Alert(
    title: "Lunchbox",
    style: alertStyle,
    desc: "The beneficiary have returned some lunchbox?",
    // ignore: prefer_const_literals_to_create_immutables
    buttons: [
      DialogButton(
          onPressed: () async {},
          child: Text(
            "No",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          height: 35.0,
          color: Colors.red),
      DialogButton(
        onPressed: () async {},
        child: Text(
          "Yes",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        height: 35.0,
        color: Colors.green,
      )
    ],
    context: context,
  ).show();
}
