// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, use_key_in_widget_constructors, avoid_unnecessary_containers

import 'package:app/model/navigation_item.dart';
import 'package:app/provider/navigation_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:app/controller/globals.dart' as globals;

MaterialColor mycolor = MaterialColor(
  0xFFE67E22,
  <int, Color>{
    50: Color(0xFFE67E22),
    100: Color(0xFFE67E22),
    200: Color(0xFFE67E22),
    300: Color(0xFFE67E22),
    400: Color(0xFFE67E22),
    500: Color(0xFFE67E22),
    600: Color(0xFFE67E22),
    700: Color(0xFFE67E22),
    800: Color(0xFFE67E22),
    900: Color(0xFFE67E22),
  },
);

class NavBar extends StatelessWidget {
  static final padding = EdgeInsets.symmetric(horizontal: 20.0);
  final imageURL =
      'https://www.rcsglobal.com/wp-content/uploads/2020/04/profile.jpg';

  @override
  Widget build(BuildContext context) => Drawer(
          child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: const SystemUiOverlayStyle(
          statusBarColor: Color.fromRGBO(230, 126, 34, 1),
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark,
        ),
        child: Container(
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              buildHeader(context,
                  urlImage: imageURL,
                  email: globals.email.toString(),
                  role: globals.role!),
              Container(
                //padding: padding,
                child: Column(children: [
                  const SizedBox(height: 24),
                  buildMenuItem(
                    context,
                    item: NavigationItem.routes,
                    text: 'Routes',
                    icon: FontAwesomeIcons.route,
                  ),
                  buildMenuItem(
                    context,
                    item: NavigationItem.beneficiaries,
                    text: 'Beneficiaries',
                    icon: Icons.people,
                  ),
                  userMenu(context),
                  pointMenu(context),
                  buildMenuItem(
                    context,
                    item: NavigationItem.logout,
                    text: 'Logout',
                    icon: Icons.logout,
                  ),
                ]),
              ),
            ],
          ),
        ),
      ));

  Widget buildMenuItem(
    BuildContext context, {
    required NavigationItem item,
    required String text,
    required IconData icon,
  }) {
    final provider = Provider.of<NavigationProvider>(context);
    final currentItem = provider.navigationItem;
    final isSelected = item == currentItem;

    return Align(
      alignment: Alignment.bottomCenter,
      child: Material(
        color: Colors.transparent,
        child: ListTile(
          selected: isSelected,
          selectedTileColor: Color.fromRGBO(230, 126, 34, 0.2),
          leading: Icon(icon),
          title: Text(text),
          onTap: () => selectItem(context, item, text),
        ),
      ),
    );
  }

  void selectItem(BuildContext context, NavigationItem item, String text) {
    final provider = Provider.of<NavigationProvider>(context, listen: false);
    provider.setNavigationItem(item);
    switch (text) {
      case "Routes":
        Navigator.of(context).pushReplacementNamed('/routes');
        break;
      case "Beneficiaries":
        Navigator.of(context).pushReplacementNamed('/beneficiary');
        break;
      case 'Logout':
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed('/');
        provider.setNavigationItem(NavigationItem.routes);
        break;
      case "Users":
        Navigator.of(context).pushReplacementNamed('/users');
        break;
      case "Points":
        Navigator.of(context).pushReplacementNamed('/point');
        break;
    }
  }

  Widget buildHeader(
    BuildContext context, {
    required String urlImage,
    required String email,
    required String role,
  }) =>
      Material(
        color: mycolor,
        child: Container(
          padding: padding.add(EdgeInsets.symmetric(vertical: 40)),
          child: Row(
            children: [
              CircleAvatar(radius: 27, backgroundImage: NetworkImage(urlImage)),
              SizedBox(width: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    email,
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    role,
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                ],
              ),
            ],
          ),
        ),
      );

  userMenu(context) {
    if (globals.role == "ADMIN") {
      return buildMenuItem(
        context,
        item: NavigationItem.user,
        text: 'Users',
        icon: FontAwesomeIcons.idBadge,
      );
    } else {
      return Container();
    }
  }

  pointMenu(context) {
    if (globals.role == "ADMIN") {
      return buildMenuItem(
        context,
        item: NavigationItem.point,
        text: 'Points',
        icon: FontAwesomeIcons.mapMarkerAlt,
      );
    } else {
      return Container();
    }
  }
}
