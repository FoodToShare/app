// ignore_for_file: no_logic_in_create_state, prefer_const_constructors, unused_import, prefer_final_fields, unnecessary_this, avoid_print, prefer_const_literals_to_create_immutables, unnecessary_new, avoid_unnecessary_containers, unnecessary_const, sized_box_for_whitespace

import 'package:app/controller/user_controller.dart';
import 'package:app/model/points_model.dart';
import 'package:app/model/routes_model.dart';
import 'package:app/model/users_model.dart';
import 'package:app/provider/MapUtils.dart';
import 'package:app/view/beneficiary_family_page.dart';
import 'package:app/view/points_page.dart';
import 'package:app/controller/routes_controller.dart';
import 'package:app/controller/points_controller.dart';
import 'package:app/view/routes_page.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:getwidget/getwidget.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:app/controller/globals.dart' as globals;

late final String teste;
bool isRoute = false;
String nameRoute = "";
String descRoute = "";
String userIdRoute = "";
String _valueUser = "0";
List _users = <UserModel>[];
MaterialColor mycolor = MaterialColor(
  0xFFE67E22,
  <int, Color>{
    50: Color(0xFFE67E22),
    100: Color(0xFFE67E22),
    200: Color(0xFFE67E22),
    300: Color(0xFFE67E22),
    400: Color(0xFFE67E22),
    500: Color(0xFFE67E22),
    600: Color(0xFFE67E22),
    700: Color(0xFFE67E22),
    800: Color(0xFFE67E22),
    900: Color(0xFFE67E22),
  },
);

class ListSimple extends StatefulWidget {
  final String pageList;

  final List deliveryPoints;
  const ListSimple(this.pageList, this.deliveryPoints, {Key? key})
      : super(key: key);

  @override
  _ListSimpleState createState() => _ListSimpleState(
      pageList: this.pageList, deliveryPoints: this.deliveryPoints);
}

class _ListSimpleState extends State<ListSimple> {
  String pageList;
  List deliveryPoints;
  _ListSimpleState({required this.pageList, required this.deliveryPoints});
  List _receivedData = List.empty();
  List<RoutesModel> _receivedRoutes = <RoutesModel>[];
  List<PointsModel> _receivedPoints = <PointsModel>[];
  List intermedia = List.empty();
  double cellphoneWidth = 0.0;

  @override
  void initState() {
    if (pageList == "Routes") {
      _users.clear();
      isRoute = true;
      RouteController().getRoutes().then((value) => {
            setState(() {
              _receivedRoutes.addAll(value);
              _receivedData = _receivedRoutes;
            })
          });
      UserController().getUsers().then((value) => {
            setState(() {
              _users.addAll(value);
            })
          });
    } else {
      isRoute = false;
      for (var i in this.deliveryPoints) {
        PointsModel point = PointsModel(
            i['id'],
            i['name'],
            i['route_id'],
            i['latitude'],
            i['longitude'],
            i['description'],
            i['firstDelivery'],
            i['addressCorrectionComment']);
        _receivedPoints.add(point);
      }
      _receivedData = _receivedPoints;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding:
          const EdgeInsets.only(top: 16.0, bottom: 0.0, left: 0.0, right: 0.0),
      child: ListView.builder(
          itemCount: _receivedData.length,
          itemBuilder: (context, index) {
            return _listItem(index, context, _receivedData[index]);
          }),
    ));
  }

  _listItem(index, context, deliveryPoints) {
    return InkWell(
      onTap: () {
        if (pageList == "Routes") {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      PointsPage(deliveryPoints.deliveryPoints)));
        } else {
          if (deliveryPoints.firstDelivery) {
            _onAlertAskForComment(context, deliveryPoints);
          } else {
            openFamilies(context, deliveryPoints);
          }
        }
      },
      child: Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.grey, width: 0.8),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: isRoute
              ? const EdgeInsets.only(
                  top: 0.0, bottom: 0.0, left: 16.0, right: 0.0)
              : const EdgeInsets.only(
                  top: 0.0, bottom: 0.0, left: 16.0, right: 0.0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: showDataList(pageList, _receivedData[index], context)),
        ),
      ),
    );
  }

  openFamilies(context, deliveryPoints) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => BeneficiaryFamilyPage(deliveryPoints.id),
        ));
  }

  showDataList(pageList, receivedData, context) {
    double widthCard = 250.0;
    Widget btn = Container();
    if (globals.role == "ADMIN") {
      widthCard = MediaQuery.of(context).size.width / 2.3;
      btn = rightData(context, receivedData);
    }
    if (pageList == "Routes") {
      return <Widget>[
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Padding(
            padding: const EdgeInsets.only(
                top: 16.0, bottom: 16.0, left: 0.0, right: 0.0),
            child: Column(
              children: [
                Text.rich(TextSpan(
                  children: [
                    WidgetSpan(
                        child: Icon(
                      FontAwesomeIcons.route,
                      color: Colors.black,
                      size: 35,
                    ))
                  ],
                )),
              ],
            ),
          ),
          SizedBox(width: 10),
          SizedBox(
            width: widthCard,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    receivedData.name,
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    receivedData.description,
                    style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        color: Colors.grey),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    receivedData.responsible.email,
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        color: Colors.grey.shade700),
                  ),
                ]),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              btn,
            ],
          )
        ])
      ];
    } else {
      return <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  top: 18.0, bottom: 18.0, left: 0.0, right: 0.0),
              child: Column(
                children: [
                  Text(
                    receivedData.name,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Column(
              children: [
                Material(
                  color: Color.fromRGBO(225, 230, 231, 1.0),
                  borderRadius: BorderRadius.circular(0.0),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 50.0,
                      child: MaterialButton(
                        onPressed: () async {
                          MapUtils.openMap(
                              receivedData.longitude, receivedData.latitude);
                        },
                        child: InkWell(
                          child: Text.rich(TextSpan(
                            children: [
                              WidgetSpan(
                                  child: Icon(
                                FontAwesomeIcons.mapMarkerAlt,
                                color: Color.fromRGBO(44, 62, 80, 1.0),
                                size: 25,
                              ))
                            ],
                          )),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        )
      ];
    }
  }

  rightData(context, receivedData) {
    if (globals.role == "VOLUNTARY") {
      /*return new CircularPercentIndicator(
        radius: 50.0,
        lineWidth: 8.0,
        animation: true,
        percent: 0.7,
        center: new Text(
          "70.0%",
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),
        ),
        circularStrokeCap: CircularStrokeCap.round,
        progressColor: mycolor,
      );*/
    } else {
      return Column(
        children: [
          Row(
            children: [
              Material(
                color: Color.fromRGBO(225, 230, 231, 1.0),
                borderRadius: BorderRadius.circular(0.0),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 50.0,
                    child: MaterialButton(
                      onPressed: () async {
                        editRoute(context, receivedData);
                      },
                      child: InkWell(
                        child: Text.rich(TextSpan(
                          children: [
                            WidgetSpan(
                                child: Icon(
                              FontAwesomeIcons.pencilAlt,
                              color: Color.fromRGBO(44, 62, 80, 1.0),
                              size: 25,
                            ))
                          ],
                        )),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 1.0,
              ),
              Material(
                color: Color.fromRGBO(225, 230, 231, 1.0),
                borderRadius: BorderRadius.circular(0.0),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 50.0,
                    child: MaterialButton(
                      onPressed: () async {
                        deleteRoute(context, receivedData);
                      },
                      child: InkWell(
                        child: Text.rich(TextSpan(
                          children: [
                            WidgetSpan(
                                child: Icon(
                              FontAwesomeIcons.trash,
                              color: Color.fromRGBO(44, 62, 80, 1.0),
                              size: 25,
                            ))
                          ],
                        )),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      );
    }
  }

  deleteRoute(context, receivedData) {
    return Alert(
        title: "Delete route",
        context: context,
        desc: "Are you sure you want to remove this route?",
        /*content: Container(
        child: Text("Are you sure you want to remove this route?", style:TextStyle(fontWeight: FontWeight.normal)),
      ),*/
        buttons: [
          DialogButton(
              onPressed: () async {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.green),
          DialogButton(
            onPressed: () async {
              Navigator.pop(context);
              await RouteController().deleteRoute(receivedData.id);
              Navigator.of(context).pushReplacementNamed('/routes');
            },
            child: Text(
              "Delete",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.red,
          )
        ]).show();
  }

  _onAlertAskForComment(context, deliveryPoints) {
    var alertStyle = AlertStyle(
      descStyle: TextStyle(fontWeight: FontWeight.normal),
      titleStyle: TextStyle(
          color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25.0),
      alertElevation: 0,
    );

    Alert(
        context: context,
        title: "Comment to delivery point",
        style: alertStyle,
        desc: "Did you have a problem finding this delivery point?",
        buttons: [
          DialogButton(
              onPressed: () async {
                PointController().addComentary(deliveryPoints.id, "");
                Navigator.pop(context);
                deliveryPoints.firstDelivery = false;
                openFamilies(context, deliveryPoints);
              },
              child: Text(
                "No",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              Navigator.pop(context);
              _onAlertPutComment(context, deliveryPoints);
            },
            child: Text(
              "Yes",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  _onAlertPutComment(context, deliveryPoints) {
    var alertStyle = AlertStyle(
      descStyle: TextStyle(fontWeight: FontWeight.normal),
      titleStyle: TextStyle(
          color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25.0),
      alertElevation: 0,
    );
    String comment = "";
    Alert(
        context: context,
        title: "Comment to delivery point",
        style: alertStyle,
        desc: "",
        content: SizedBox(
          height: 90,
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Put your commentary here!',
            ),
            expands: true,
            maxLines: null,
            onChanged: (value) {
              comment = value;
            },
          ),
        ),
        buttons: [
          DialogButton(
              onPressed: () async {
                Navigator.pop(context);
                _onAlertAskForComment(context, deliveryPoints);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              PointController().addComentary(deliveryPoints.id, comment);
              deliveryPoints.firstDelivery = false;
              Navigator.pop(context);
              openFamilies(context, deliveryPoints);
            },
            child: Text(
              "Save",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  editRoute(context, route) {
    var name = new TextEditingController(text: route.name);
    var desc = new TextEditingController(text: route.description);
    nameRoute = route.name;
    descRoute = route.description;

    //  if()
    _valueUser = route.responsible.id.toString();
    Alert(
        context: context,
        title: "Edit route",
        content: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          child: Column(
            children: [
              SizedBox(
                height: 10.0,
              ),
              TextField(
                controller: name,
                style: TextStyle(fontSize: 15.0, height: 0.5),
                decoration: InputDecoration(
                  labelText: "Route name",
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(5.0),
                    ),
                  ),
                ),
                onChanged: (value) {
                  nameRoute = value;
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              //----------------------------
              TextField(
                controller: desc,
                style: TextStyle(fontSize: 15.0, height: 0.5),
                onChanged: (value) {
                  descRoute = value;
                },
                decoration: InputDecoration(
                  labelText: "Route description",
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(5.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              //----------------------------
              Container(
                height: 53.0,
                //constraints: BoxConstraints(maxHeight: 50.0, minHeight: 40.0),
                child: DropdownButtonFormField<String>(
                  items: prepareData(),
                  itemHeight: null,
                  value: _valueUser,
                  isExpanded: true,
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 30.0,
                      color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "User",
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(5.0),
                      ),
                    ),
                  ),
                  onChanged: (value) => setState(() {
                    _valueUser = value!;
                  }),
                  hint: Text("Select user"),
                ),
              ),
            ],
          ),
        ),
        buttons: [
          DialogButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              await RouteController().editRoute(
                  nameRoute, descRoute, int.tryParse(_valueUser)!, route.id);
              Navigator.pop(context);
              showToast("Route edited successfully!");
              Navigator.of(context).pushReplacementNamed('/routes');
            },
            child: Text(
              "Edit",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  prepareData() {
    List<DropdownMenuItem<String>> items = <DropdownMenuItem<String>>[];
    items.add(DropdownMenuItem(
      value: (0).toString(),
      child: Text(
        "Dont associate any user.",
        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
      ),
    ));
    for (var i in _users) {
      items.add(DropdownMenuItem(
        value: i.id.toString(),
        child: Text(
          i.email,
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
        ),
      ));
    }
    return items;
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
