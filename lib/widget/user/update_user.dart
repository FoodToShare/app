// ignore_for_file: unused_element, prefer_const_constructors, unnecessary_new, sized_box_for_whitespace, unnecessary_const

import 'package:app/controller/user_controller.dart';
import 'package:app/model/roles_item.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class UpdateUser extends StatefulWidget {
  const UpdateUser({Key? key}) : super(key: key);

  @override
  _UpdateUserState createState() => _UpdateUserState();
}

class _UpdateUserState extends State<UpdateUser> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }

  modalCreateUser(context, user) {
    var emailV = new TextEditingController(text: user.email);
    String role = user.role;
    String email = user.email;
    String password = "";
    List<DropdownMenuItem<String>> roles = <DropdownMenuItem<String>>[];
    for (var v in RolesItem.values) {
      roles.add(DropdownMenuItem(
        value: EnumToString.convertToString(v),
        child: Text(
          EnumToString.convertToString(v),
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
        ),
      ));
    }
    Alert(
        context: context,
        title: "Create user",
        content: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          child: Column(
            children: [
              SizedBox(
                height: 10.0,
              ),
              TextField(
                controller: emailV,
                style: TextStyle(fontSize: 15.0, height: 0.5),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: 'Email',
                  border: OutlineInputBorder(),
                ),
                onChanged: (value) {
                  email = value;
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                obscureText: true,
                keyboardType: TextInputType.text,
                style: TextStyle(fontSize: 15.0, height: 0.5),
                decoration: InputDecoration(
                  labelText: 'Password',
                  border: OutlineInputBorder(),
                ),
                onChanged: (value) {
                  password = value;
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                height: 53.0,
                child: DropdownButtonFormField<String>(
                  items: roles,
                  value: role,
                  isExpanded: true,
                  onChanged: (value) => setState(() {
                    role = value!;
                  }),
                  hint: Text("Select user"),
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 30.0,
                      color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "User",
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        buttons: [
          DialogButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              if (email == "" || role == "") {
                showToast("All fields are mandatory");
              } else {
                await updateUser(email, password, role, user.id, context);
              }
            },
            child: Text(
              "Update",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  updateUser(email, password, role, userID, context) async {
    String response =
        await UserController().updateUser(email, password, role, userID);
    if (response == "success") {
      showToast("User upodated successfully");
      Navigator.of(context).pushReplacementNamed('/users');
    } else {
      showToast("Error creating user");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
