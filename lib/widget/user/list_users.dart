// ignore_for_file: prefer_const_constructors

import 'package:app/controller/user_controller.dart';
import 'package:app/model/users_model.dart';
import 'package:app/widget/user/delete_users.dart';
import 'package:app/widget/user/update_user.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

List _users = <UserModel>[];

class ListUsers extends StatefulWidget {
  const ListUsers({Key? key}) : super(key: key);

  @override
  _ListUsersState createState() => _ListUsersState();
}

class _ListUsersState extends State<ListUsers> {
  @override
  void initState() {
    _users.clear();
    UserController().getUsers().then((value) => {
          setState(() {
            _users.addAll(value);
          })
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding:
          const EdgeInsets.only(top: 16.0, bottom: 0.0, left: 0.0, right: 0.0),
      child: ListView.builder(
          itemCount: _users.length,
          itemBuilder: (context, index) {
            return _listUser(index, context, _users[index]);
          }),
    ));
  }

  _listUser(index, content, user) {
    return InkWell(
        onTap: () {},
        child: Card(
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.grey, width: 0.8),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                top: 0.0, bottom: 0.0, left: 16.0, right: 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16.0, bottom: 16.0, left: 0.0, right: 0.0),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.userAlt,
                            color: Colors.black,
                            size: 35,
                          ))
                        ],
                      )),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(user.email),
                    Text(
                      user.role,
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    buttonsList(context, user),
                  ],
                )
              ],
            ),
          ),
        ));
  }

  buttonsList(context, user) {
    return Column(
      children: [
        Row(
          children: [
            Material(
              color: Color.fromRGBO(225, 230, 231, 1.0),
              borderRadius: BorderRadius.circular(0.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 50.0,
                  child: MaterialButton(
                    onPressed: () async {
                      await UpdateUser()
                          .createState()
                          .modalCreateUser(context, user);
                      //editUser(context, user);
                    },
                    child: InkWell(
                      child: Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.userEdit,
                            color: Color.fromRGBO(44, 62, 80, 1.0),
                            size: 25,
                          ))
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 1.0,
            ),
            Material(
              color: Color.fromRGBO(225, 230, 231, 1.0),
              borderRadius: BorderRadius.circular(0.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 50.0,
                  child: MaterialButton(
                    onPressed: () async {
                      await DeleteUser(user.id)
                          .modalDeleteUser(context, user.id);
                    },
                    child: InkWell(
                      child: Text.rich(TextSpan(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.userMinus,
                            color: Color.fromRGBO(44, 62, 80, 1.0),
                            size: 25,
                          ))
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
