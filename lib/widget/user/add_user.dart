// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, sized_box_for_whitespace, unnecessary_const

import 'package:app/controller/user_controller.dart';
import 'package:app/model/roles_item.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:enum_to_string/enum_to_string.dart';

MaterialColor mycolor = MaterialColor(
  0xFFE67E22,
  <int, Color>{
    50: Color(0xFFE67E22),
    100: Color(0xFFE67E22),
    200: Color(0xFFE67E22),
    300: Color(0xFFE67E22),
    400: Color(0xFFE67E22),
    500: Color(0xFFE67E22),
    600: Color(0xFFE67E22),
    700: Color(0xFFE67E22),
    800: Color(0xFFE67E22),
    900: Color(0xFFE67E22),
  },
);
String email = "";
String password = "";
String role = "VOLUNTARY";

class AddUser extends StatefulWidget {
  const AddUser({Key? key}) : super(key: key);

  @override
  _AddUserState createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        _modalCreateUser(context);
      },
      child: Icon(FontAwesomeIcons.userPlus),
      backgroundColor: mycolor,
    );
  }

  _modalCreateUser(context) {
    List<DropdownMenuItem<String>> roles = <DropdownMenuItem<String>>[];
    for (var v in RolesItem.values) {
      roles.add(DropdownMenuItem(
        value: EnumToString.convertToString(v),
        child: Text(
          EnumToString.convertToString(v),
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.0),
        ),
      ));
    }
    Alert(
        context: context,
        title: "Create user",
        content: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          child: Column(
            children: [
              SizedBox(
                height: 10.0,
              ),
              TextField(
                style: TextStyle(fontSize: 15.0, height: 0.5),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: 'Email',
                  border: OutlineInputBorder(),
                ),
                onChanged: (value) {
                  email = value;
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                obscureText: true,
                keyboardType: TextInputType.text,
                style: TextStyle(fontSize: 15.0, height: 0.5),
                decoration: InputDecoration(
                  labelText: 'Password',
                  border: OutlineInputBorder(),
                ),
                onChanged: (value) {
                  password = value;
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                height: 53.0,
                child: DropdownButtonFormField<String>(
                  items: roles,
                  value: role,
                  isExpanded: true,
                  onChanged: (value) => setState(() {
                    role = value!;
                  }),
                  hint: Text("Select role"),
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 30.0,
                      color: Colors.black),
                  decoration: InputDecoration(
                    labelText: "Role",
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        buttons: [
          DialogButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.red),
          DialogButton(
            onPressed: () async {
              if (email == "" || password == "" || role == "") {
                showToast("All fields are mandatory");
              } else {
                await createUser(email, password, role);
              }
            },
            child: Text(
              "Create",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.green,
          )
        ]).show();
  }

  createUser(email, password, role) async {
    String response = await UserController().createUser(email, password, role);
    if (response == "success") {
      showToast("User created successfully");
      Navigator.of(context).pushReplacementNamed('/users');
    } else {
      showToast("Error creating user");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
