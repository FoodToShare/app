// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unnecessary_this, no_logic_in_create_state

import 'package:app/controller/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class DeleteUser extends StatelessWidget {
  final int userID;
  const DeleteUser(this.userID, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return modalDeleteUser(context, this.userID);
  }

  modalDeleteUser(context, idUser) {
    return Alert(
        title: "Delete user",
        context: context,
        desc: "Are you sure you want to remove this user?",
        buttons: [
          DialogButton(
              onPressed: () async {
                Navigator.pop(context);
              },
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              height: 35.0,
              color: Colors.green),
          DialogButton(
            onPressed: () async {
              deleteUser(this.userID, context);
            },
            child: Text(
              "Delete",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            height: 35.0,
            color: Colors.red,
          )
        ]).show();
  }

  deleteUser(idUser, context) async {
    String response = await UserController().deleteUser(idUser);
    if (response == "success") {
      showToast("User deleted successfully");
      Navigator.of(context).pushReplacementNamed('/users');
    } else {
      showToast("Error  when deleting user");
    }
  }

  void showToast(text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
