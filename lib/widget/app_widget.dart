// ignore_for_file: prefer_const_constructors

import 'package:app/view/routes_page.dart';
import 'package:app/view/users_page.dart';
import 'package:flutter/material.dart';
import 'package:app/view/beneficiary_page.dart';
import 'package:app/view/home_page.dart';
import 'package:app/view/login_page.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(),
        '/home': (context) => HomePage(),
        '/beneficiary': (context) => BeneficiarioPage(),
        '/routes': (context) => RoutePage(),
        '/users': (context) => UsersPage(),
      },
    );
  }
}
