// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, prefer_const_literals_to_create_immutables, must_be_immutable

import 'package:app/model/navigation_item.dart';
import 'package:app/provider/navigation_provider.dart';
import 'package:app/view/point_page.dart';
import 'package:app/view/routes_page.dart';
import 'package:app/view/users_page.dart';
import 'package:flutter/material.dart';
import 'package:app/view/beneficiary_page.dart';
import 'package:app/view/home_page.dart';
import 'package:app/view/login_page.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:app/controller/login_controller.dart' as controller;

MaterialColor mycolor = MaterialColor(
  0xFFE67E22,
  <int, Color>{
    50: Color(0xFFE67E22),
    100: Color(0xFFE67E22),
    200: Color(0xFFE67E22),
    300: Color(0xFFE67E22),
    400: Color(0xFFE67E22),
    500: Color(0xFFE67E22),
    600: Color(0xFFE67E22),
    700: Color(0xFFE67E22),
    800: Color(0xFFE67E22),
    900: Color(0xFFE67E22),
  },
);

class AppWidget extends StatelessWidget {
  // const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => NavigationProvider(),
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: const SystemUiOverlayStyle(
          statusBarColor: Color.fromRGBO(230, 126, 34, 1),
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark,
        ),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              primarySwatch: mycolor,
              appBarTheme: AppBarTheme(
                  systemOverlayStyle: SystemUiOverlayStyle.light,
                  color: mycolor)),
          initialRoute: '/',
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          routes: {
            '/': (context) => LoginPage(),
            '/home': (context) => HomePage(),
            '/beneficiary': (context) => BeneficiarioPage(),
            '/routes': (context) => RoutePage(),
            '/users': (context) => UsersPage(),
            '/point': (context) => PointPage(),
          },
        ),
      ),
    );
  }
}

String selected(BuildContext context) {
  final provider = Provider.of<NavigationProvider>(context);
  final navigationItem = provider.navigationItem;

  switch (navigationItem) {
    case NavigationItem.routes:
      return "/routes";
    case NavigationItem.beneficiaries:
      return '/beneficiary';
    case NavigationItem.logout:
      return logout();
    case NavigationItem.user:
      return '/users';
    case NavigationItem.point:
      return '/point';
  }
}

logout() async {
  var response = await controller.LoginController().logout();
  if (response == 'Success') {
    return '/';
  } else {
    return '/';
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) => buildPages();

  Widget buildPages() {
    final provider = Provider.of<NavigationProvider>(context);
    final navigationItem = provider.navigationItem;

    switch (navigationItem) {
      case NavigationItem.routes:
        return RoutePage();
      case NavigationItem.beneficiaries:
        return BeneficiarioPage();
      case NavigationItem.logout:
        return logout();
      case NavigationItem.user:
        return UsersPage();
      case NavigationItem.point:
        return PointPage();
    }
  }
}

/*logout() async {
  var response = await controller.LoginController().logout();
  if (response == 'Success') {
    return LoginPage();
  } else {
    return LoginPage();
  }
}*/
